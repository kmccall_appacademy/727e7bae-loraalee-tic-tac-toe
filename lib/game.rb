require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require 'byebug'

class Game
  attr_reader :board, :mark, :player_one, :player_two
  attr_accessor :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
    @board = Board.new
  end

  def switch_players!
    if current_player == player_one
      @current_player = player_two
    else
      @current_player = player_one
    end
  end

  def play_turn
    pos = current_player.get_move
    while !board.empty?(pos)
      puts "That space is already taken. Enter a new move"
      pos = current_player.get_move
    end

    board.place_mark(pos, current_player.mark)

    puts "#{current_player.name}'s move:"
    player_one.display(board)
    player_two.display(board)

    switch_players!
  end

  def play
    puts "Board:"
    player_one.display(board)
    player_two.display(board)

    until board.over?
      play_turn
    end

    if board.winner && board.winner == :X
      puts "You lose"
    elsif board.winner && board.winner == :O
      puts "Congratulations, you win!"
    else
      puts "It's a tie!"
    end
  end

end

if $PROGRAM_NAME == __FILE__

  print "Enter your name: "
  name = gets.chomp
  human = HumanPlayer.new(name)
  tom = ComputerPlayer.new('Tom')

  new_game = Game.new(human, tom)
  new_game.play
end
