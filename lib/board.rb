class Board
  attr_reader :grid

  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def place_mark(pos, mark)
    grid[pos[0]][pos[1]] = mark
  end

  def [](i)
    grid[i]
  end

  def empty?(pos = [])
    if pos != []
      if grid[pos[0]][pos[1]] == nil
        true
      else
        false
      end
    else
      (0..2).each do |row|
        (0..2).each do |col|
          if grid[row][col] != nil
            return false
          end
        end
      end
      true
    end
  end

  def winner
    if row_winner
      row_winner
    elsif left_diag_winner
      left_diag_winner
    elsif right_diag_winner
      right_diag_winner
    elsif col_winner
      col_winner
    end
  end

  def over?
    if self.empty?
      false
    elsif winner
      true
    elsif full? && !winner
      true
    else
      false
    end
  end

  def full?
    (0..2).each do |row|
      (0..2).each do |col|
        if grid[row][col] == nil
          return false
        end
      end
    end
    true
  end

  private

  def row_winner
    (0..2).each do |row|
      if grid[row].all? { |space| space == :X }
        return :X
      elsif grid[row].all? { |space| space == :O }
        return :O
      end
    end

    false
  end

  def left_diag_winner
    marks = []

    (0..2).each do |i|
      marks << grid[i][i]
    end

    if marks.all? { |mark| mark == :X }
      return :X
    elsif marks.all? { |mark| mark == :O }
      return :O
    else
      false
    end
  end

  def right_diag_winner
    marks = []
    cols = [2, 1, 0]

    (0..2).each do |i|
      marks << grid[i][cols[i]]
    end

    if marks.all? { |mark| mark == :X }
      return :X
    elsif marks.all? { |mark| mark == :O }
      return :O
    else
      false
    end
  end

  def col_winner
    marks = []
    col = 0

    3.times do
      (0..2).each do |row|
        marks << grid[row][col]
      end
      col += 1

      if marks.all? { |mark| mark == :X }
        return :X
      elsif marks.all? { |mark| mark == :O }
        return :O
      else
        marks = []
      end
    end

    false
  end

end
