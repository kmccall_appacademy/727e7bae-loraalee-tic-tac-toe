class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    rows = []

    (0..2).each do |row|
      board.grid[row].each do |space|
        if space == nil
          rows << "   "
        else
          rows << " #{space} "
        end
      end
      puts rows.join("|")
      rows = []
      puts "-----------" unless row == 2
    end

  end

  def get_move
    puts "Where would you like to move? (row, col)"
    pos = gets.chomp.split(", ")
    pos.map { |num| num.to_i - 1 }
  end

end
