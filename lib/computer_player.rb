class ComputerPlayer
  attr_reader :board, :name
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :X
  end

  def display(board)
    @board = board
  end

  def get_move
    if col_win_move
      col_win_move
    elsif row_win_move
      row_win_move
    elsif left_diag_win_move
      left_diag_win_move
    elsif right_diag_win_move
      right_diag_win_move
    else
      get_random_move
    end
  end

  def get_random_move
    empty = false

    while empty == false
      row = rand(3)
      col = rand(3)

      if @board.empty?([row, col])
        empty = true
        return [row, col]
      end
    end
  end

  private

  def row_win_move
    marks = []
    pos = []

    (0..2).each do |row|
      @board.grid[row].each_with_index do |mark, col|
        marks << mark
        pos << [row, col]
      end

      if marks.count(nil) == 1 && marks.count(:X) == 2 
        i = marks.index(nil)
        return pos[i]
      else
        marks = []
        pos = []
      end
    end

    false
  end

  def col_win_move
    marks = []
    pos = []
    col = 0

    3.times do
      (0..2).each do |row|
        marks << @board.grid[row][col]
        pos << [row, col]
      end
      col += 1

      if marks.count(nil) == 1 && marks.count(:X) == 2
        i = marks.index(nil)
        return pos[i]
      else
        marks = []
        pos = []
      end
    end

    false
  end

  def left_diag_win_move
    marks = []
    pos = []

    (0..2).each do |i|
      marks << @board.grid[i][i]
      pos << [i, i]
    end

    if marks.count(nil) == 1 && marks.count(:X) == 2
      i = marks.index(nil)
      pos[i]
    else
      false
    end
  end

  def right_diag_win_move
    marks = []
    pos = []
    cols = [2, 1, 0]

    (0..2).each do |i|
      marks << @board.grid[i][cols[i]]
      pos << [i, cols[i]]
    end

    if marks.count(nil) == 1 && marks.count(:X) == 2
      i = marks.index(nil)
      pos[i]
    else
      false
    end
  end

end
